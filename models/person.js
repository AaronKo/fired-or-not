var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Person = mongoose.model('person',
    {
        name: String,
        file: String,
        fire: Number,
        stay: Number
    });

module.exports = Person;