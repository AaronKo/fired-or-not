var express = require('express');
var router = express.Router();
var request = require('request');
var Person = require('../models/person');


/* GET home page. */
router.get('/', function(req, res, next) {
  Person.find({}, function(err, doc){
    if (doc.length == 0){
      request('https://gist.githubusercontent.com/thireven/5e03140e2a1834893186591fa036b5a2/raw/firedornot.json', function (error, response, body) {
        if (!error && response.statusCode == 200) {
          var newBody = JSON.parse(body);
          console.log(newBody);
          console.log(newBody.length);
          for (var i = 0; i<newBody.length; i++){
              var newPerson = new Person({
                name: newBody[i].name,
                file: newBody[i].file,
                fire: 0,
                stay: 0
              });
              newPerson.save(function(err, user){
                if (err) throw (err);
              });
          }
        }
      });
    }
    Person.find({}, function(err, doc){
      res.render('index', { persons: doc });
    });
  });
});

router.post('/', function(req, res){
    Person.findOne({name: req.body.name}, function(err, doc){
      console.log(doc);
      if(err) throw (err);
      var updated = {

      };
      if (req.body.action == "fire"){
          Person.update({_id: doc._id}, {fire: doc.fire+1}, function(err, doc){

          });
      }
      else{
        Person.update({_id: doc._id}, {stay: doc.stay+1}, function(err, doc){

        });
      }
      res.redirect('/');
    });
});

router.get('/stats', function(req, res){
  Person.find({}, function(err, doc){
    if (doc.length == 0){
      res.redirect('/');
    }
    else{
      res.render('stats', { persons: doc });
    }
  });
});

module.exports = router;
